#include <SoftwareSerial.h>

////// Pinnar för software serial:
SoftwareSerial monitor(6, 5); // RX, TX
#define wifi Serial
//#define monitor Serial

char serialbuffer[1000];//serial buffer for request url

void setup()
{
//////// Baudrate för esp och för monitorn
  wifi.begin(19200);//connection to ESP8266
  monitor.begin(9600); //serial debug
  
  
  monitor.println("AAAAA");
  //set mode needed for new boards
  wifi.println("AT+RST");
  wifi.println("AT+CWMODE=1");
  delay(500);//delay after mode change
  wifi.println("AT+RST");
  
  //connect to wifi network
  ////////// wifi-nät och lösenord
  wifi.println("AT+CWJAP=\"hejhej\",\"ingenting\"");
  monitor.println("BBBBB");
}

void loop()
{
  //output everything from ESP8266 to the Arduino Micro Serial output
  while (wifi.available() > 0) {
  monitor.write(wifi.read());
  }
  
  if (monitor.available() > 0) {
     //read from serial until terminating character
     int len = monitor.readBytesUntil('\n', serialbuffer, sizeof(serialbuffer));
  
     //trim buffer to length of the actual message
     String message = String(serialbuffer).substring(0,len-1);
     monitor.println("message: " + message);
 
     //check to see if the incoming serial message is a url or an AT command
     if(message.substring(0,2)=="AT"){
       //make command request
       monitor.println("COMMAND REQUEST");
       wifi.println(message); 
     }else{
      //make webrequest
       monitor.println("WEB REQUEST");
       WebRequest(message);
     }
  }
}

//web request needs to be sent without the http for now, https still needs some working
void WebRequest(String request){
 //find the dividing marker between domain and path
     int slash = request.indexOf('/');
     
     //grab the domain
     String domain;
     if(slash>0){  
       domain = request.substring(0,slash);
     }else{
       domain = request;
     }

     //get the path
     String path;
     if(slash>0){  
       path = request.substring(slash);   
     }else{
       path = "/";          
     }
     
     //output domain and path to verify
     monitor.println("domain: |" + domain + "|");
     monitor.println("path: |" + path + "|");     
     
     //create start command
     String startcommand = "AT+CIPSTART=\"TCP\",\"" + domain + "\", 80"; //443 is HTTPS, still to do
     
     wifi.println(startcommand);
     //monitor.println(startcommand);
     
     
     //test for a start error
     if(wifi.find("Error")){
       monitor.println("error on start");
       return;
     }
     
     //create the request command
     String sendcommand = "GET http://"+ domain + path + " HTTP/1.0\r\n\r\n\r\n";//works for most cases
     
     monitor.print(sendcommand);
     
     //send 
     wifi.print("AT+CIPSEND=");
     wifi.println(sendcommand.length());
     
     //debug the command
     monitor.print("AT+CIPSEND=");
     monitor.println(sendcommand.length());
     
     delay(5000);
     if(wifi.find(">"))
     {
       monitor.println(">");
     }else
     {
       wifi.println("AT+CIPCLOSE");
       monitor.println("connect timeout");
       delay(1000);
       return;
     }
     
     //monitor.print(getcommand);
     wifi.print(sendcommand); 
}
