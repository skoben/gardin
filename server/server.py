from flask import Flask, render_template, url_for, jsonify

app = Flask(__name__)

requested_state = {'front': {}, 'back': {}}
current_state = {'front': {}, 'back': {}}

@app.route('/set_position/<side>/<int:bottom>,<int:top>')
def set_position(side, top, bottom):
    global requested_state, current_state

    requested_state[side] = {'top': top, 'bottom': bottom}
    return ''


@app.route('/current_positions')
def current_positions():
    global current_state, requested_state
    print(current_state, requested_state)

    return jsonify(current_state)


@app.route('/position/<side>/<end>/<int:position>')
def position(side, end, position):
    global requested_state, current_state

    current_state[side][end] = position
    requested_position = requested_state.get(side).get(end)
    return 'pos={}\n'.format(requested_position or '')


@app.route('/')
def main_page():
    return render_template('gardin.html')
 
if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
