#include <SoftwareSerial.h>

//Position 
int Position;
int NewPosition;
int RequestedPosition = 0;
int MaxPosition; 
int MinPosition=0;
int inbyte;
int digitalState;
int pinNumber;
int analogRate;
int sensorVal;

char buf[256];

//erik
int CurrentColor, OldColor, ReadColor, CurrentPosition, SensorMiddleValue;
unsigned long StartTime, ReadTime, ElapsedTime, LimitTime;


int TurnCounter;
unsigned long start, finished, elapsed;
int oldstate=0;
int state= 0;
int Step =0; // READY program case-switch funtion
int Start =0;// START Mätningsprogram 
//Sensors
int SensorLeft =A0;
int SensorMiddle =A1;
int SensorRight =A2;
int SensorIR =A3;
int ValueSensorRight;
int ValueSensorMiddle;
int ValueSensorLeft;
static int DireccionSensorMiddle=1;
int ValueSensorIR;
int OutputValueSensorRight;
int OutputValueSensorMiddle;
int OutputValueSensorLeft;
int OutputValueSensorIR;
static int MaxCount = 0;
int ReadSensorIR;
int ReadMotorUp;
int ReadMotorDown;
//Motors
int Motor1Pin = 5;   
int Motor2Pin = 6;
int Rotation;

SoftwareSerial monitor(11, 10); // RX, TX
#define wifi Serial

void setup(){
  monitor.begin(115200);
  wifi.begin(19200);
  wifi.println("AT+RST");
  wifi.println("AT+CWMODE=1");
  delay(500);
  wifi.println("AT+RST");

  //connect to wifi network
  wifi.println("AT+CWJAP=\"funkarinte\",\"p241p40w\"");
  delay(5000);
  monitor.println("XXXXXXXXXXXXXXXXXX");

  LimitTime = 1500;
  start = millis();
pinMode(Motor1Pin, OUTPUT);
pinMode(Motor2Pin, OUTPUT);
pinMode(SensorIR, INPUT);
pinMode(SensorRight,INPUT); // analog input
pinMode(SensorMiddle,INPUT);
pinMode(SensorLeft,INPUT);
}

void loop() {
	if (millis() % 5000 == 0) {
		monitor.println("WEBREQUEST");
		WebRequest();
	}
   
   switch(Start) {           	
        case 0:
         monitor.println("Performing Start 0");
         ReadColor = analogRead(A3);
         if(ReadColor < 10) {
            OldColor = 1;
         }
         else {
            OldColor = 0;
         }
         MotorUp();
         StartTime = millis();
         Start = 1;
       break;
        case 1:
         monitor.println("1111111111111");
         MotorStop();
         ReadColor = analogRead(A3);
         if(ReadColor < 100) {
            CurrentColor = 1;
         }
         else {
            CurrentColor = 0;
         }
         if(OldColor == CurrentColor) {
            ReadTime = millis();
            ElapsedTime = ReadTime - StartTime;
            if(ElapsedTime > LimitTime) {            
               MotorStop();
               monitor.println("No Rotation Detected!"); 
               monitor.print("Elapsed Time: ");
               monitor.println(ElapsedTime);
               delay(3000);
               Start = 2;  
            }
            else {
               MotorUp();
            }
         }
         else {
            OldColor = CurrentColor;
            StartTime = millis();
            MotorUp();
         }
		break;
        case 2:
         monitor.println("Performing Start 2");
         ReadColor = analogRead(A3);
         if(ReadColor < 100) {
            OldColor = 1;
         }
         else {
            OldColor = 0;
         }
         MotorDown();
         StartTime = millis();
         Start = 3;         
		break;
        case 3:
         monitor.println("Performing Start 3");
         monitor.print("MaxPosition: ");
         monitor.println(MaxPosition);
         MotorStop();
         ReadColor = analogRead(A3);
         if(ReadColor < 100) {
            CurrentColor = 1;
         }
         else {
            CurrentColor = 0;
         }
         if(OldColor == CurrentColor) {
            ReadTime = millis();
            ElapsedTime = ReadTime - StartTime;
            if(ElapsedTime > LimitTime) {            
               MotorStop();
			   //delay(2000);
               monitor.println("No Rotation Detected!"); 
               monitor.print("Elapsed Time: ");
               monitor.println(ElapsedTime);
               CurrentPosition = 0;
               Start = 4;  
			   
            }
            else {
               MotorDown();
            }
         }
         else {
            OldColor = CurrentColor;
            StartTime = millis();
            MaxPosition++;
            MotorDown();
         }
			break;
        case 4:
                  monitor.println("Start 4: Ready ");
                  //Position = TurnCounter;
                  monitor.print("MaxPosition:"); //For Debugging
                  monitor.println(MaxPosition); //For Debugging
                  monitor.print("CurrentPosition:"); //For Debugging
                  monitor.println(CurrentPosition); //For Debugging
		          checkForNewPosition();
				if (RequestedPosition <=100){
					NewPosition = MaxPosition * (RequestedPosition * 0.01);
					monitor.print("NewPosition: ");
					monitor.println(NewPosition);
					if( NewPosition>=0 ) {
						if(CurrentPosition < NewPosition) {
								MotorStop();
								ReadColor = analogRead(A3);
								if(ReadColor > 300) {
									OldColor = 1;
								}
								else {
									OldColor = 0;
								}
								monitor.println(" Going UP to new position"); //For Debugging
								MotorUp();
								StartTime = millis();
								Start = 5;   
						}
						if(CurrentPosition > NewPosition) { 
								MotorStop();
								ReadColor = analogRead(A3);
								if(ReadColor > 300) {
									OldColor = 1;
								}
								else {
									OldColor = 0;
								}
								monitor.println("Going down to new position"); //For Debugging
								MotorDown();
								StartTime = millis();
								Start = 6;
						}
					}
				}
                            else if (RequestedPosition == 101) {
                            Start=7;
                            }
			    else {
					Start = 4;
				}
					break;
					
		case 5:   
         monitor.println("Going UP to new position and counting");
         monitor.print("MaxPosition: ");
         monitor.println(MaxPosition);
         monitor.print("CurrentPosition: ");
         monitor.println(CurrentPosition); 
		 monitor.print("NewPosition: ");
		 monitor.println(NewPosition);		 
            if(CurrentPosition == MaxPosition) {
               MotorStop();
               monitor.println("Current Position Top limit");  
				Start=4;
            }
            else {
               MotorStop();
               ReadColor = analogRead(A3);
               if(ReadColor < 100) {
                  CurrentColor = 1;
               }
               else {
                  CurrentColor = 0;
               }
               if(CurrentColor == OldColor) {
                  ReadTime = millis();
                  ElapsedTime = ReadTime - StartTime;
                  if(ElapsedTime > LimitTime) {
					  monitor.println("No Rotation Detected");
					  MotorStop();
					  delay(2000);
						Start=4;
                    }
                  else {
                     MotorUp();
                    }
                }
               else {
                  OldColor = CurrentColor;
                  CurrentPosition++;
                  StartTime = millis();
                  MotorUp();
                }
            }
         if (CurrentPosition == NewPosition) {
						monitor.println(" Position == New Position");
			MotorStop();			
			Start=4;
			}
      break; //Start:5
	  
	    case 6:   
         monitor.println("Going UP to new position and counting");
         monitor.print("MaxPosition: ");
         monitor.println(MaxPosition);
         monitor.print("CurrentPosition: ");
         monitor.println(CurrentPosition); 
		 monitor.print("NewPosition: ");
		 monitor.println(NewPosition);		 
            if(CurrentPosition == 0) {
               MotorStop();
               monitor.println("Current Position ground limit");  
				Start=4;
            }
            else {
              MotorStop();
               ReadColor = analogRead(A3);
               if(ReadColor < 100) {
                  CurrentColor = 1;
                }
               else {
                  CurrentColor = 0;
                } 
					if(CurrentColor == OldColor) {
						ReadTime = millis();
						ElapsedTime = ReadTime - StartTime;
						if(ElapsedTime > LimitTime) {
							monitor.println("No Rotation Detected");
							MotorStop();
							delay(2000);
							Start=4;
						}
						else {
							MotorDown();
						}
					}
					else {
						OldColor = CurrentColor;
						CurrentPosition--;
						StartTime = millis();
						MotorDown();
					}
			}
           if (CurrentPosition == NewPosition) {
						monitor.println(" Position == New Position");
						MotorStop();					
						Start=4;
		    }
			break; //Start:6
			
		case 7:
                  monitor.println("Step 0: Ready ");
                  Position = TurnCounter;
                  monitor.print("MaxPosition:"); //For Debugging
                  monitor.println(MaxPosition); //For Debugging
                  monitor.print("CurrentPosition:"); //For Debugging
                  monitor.println(CurrentPosition); //For Debugging
         
			switch(Step) {           	
               case 0:		// 	Startrutin	
                  ReadSensors();
                    if(OutputValueSensorLeft > 300) { //SENSOR LEFT STEPS
						if(CurrentPosition == MaxPosition) {
							MotorStop();
							monitor.println("Current Position Top limit");
							Step = 0;
						}
						else {
							MotorStop();
							ReadColor = analogRead(A3);
							if(ReadColor > 300) {
								OldColor = 1;
							}
							else {
								OldColor = 0;
							}
							MotorUp();
							StartTime = millis();
							Step = 1;
						}
					}  

                  else if(OutputValueSensorMiddle > 300 && DireccionSensorMiddle == 1) { //SENSOR MIDDLE STEPS
						delay(500);
						if(CurrentPosition == MaxPosition) {
							MotorStop();
							monitor.println("Current Position Top limit");
							DireccionSensorMiddle =0;
							Step = 12;
						}
						else {
							//MotorStop();
							ReadColor = analogRead(A3);
							if(ReadColor > 300) {
								OldColor = 1;
							}
							else {
								OldColor = 0;
							}
							MotorUp();
							StartTime = millis();
							Step = 11;
						}
					}  
					else if(OutputValueSensorMiddle > 300 && DireccionSensorMiddle == 0) { //SENSOR MIDDLE STEPS
						delay(500);
						if(CurrentPosition == 0) {
							MotorStop();
							monitor.println("Current Position  ground Limit");
							DireccionSensorMiddle = 1;
							Step = 11;
						}
						else {
							MotorStop();
							ReadColor = analogRead(A3);
							if(ReadColor > 300) {
								OldColor = 1;
							}
							else {
								OldColor = 0;
							}
							MotorDown();
							StartTime = millis();
							Step = 12;
						}                 
					}
					
					
                  else if(OutputValueSensorRight > 300) { //SENSOR RIGHT STEPS
						if(CurrentPosition == 0) {
							MotorStop();
							monitor.println("Current Position ground Limit");
							Step = 0;
						}
						else {
							MotorStop();
							ReadColor = analogRead(A3);
							if(ReadColor > 300) {
								OldColor = 1;
							}
							else {
								OldColor = 0;
							}
							MotorUp();
							StartTime = millis();
							Step = 21;
						}
					} 
					else if(OutputValueSensorRight < 10 &&  OutputValueSensorLeft < 10  ) { //SENSOR RIGHT and sensor left are blackned
						delay(2000);
						Start =4;
					} 
					else {
						monitor.println(" No Light Detected");
                    }     
					break; // case : 0
      
				case 1:   
					monitor.println("Performing Left sensor ON = Going UP");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition);  
					ReadSensors();
				if(OutputValueSensorLeft > 300) {
					if(CurrentPosition == MaxPosition) {
						MotorStop();
						monitor.println("Current Position limit");  
						Step=0;
					}
					else {
						MotorStop();
						ReadColor = analogRead(A3);
						if(ReadColor < 100) {
							CurrentColor = 1;
						}
						else {
							CurrentColor = 0;
						}
						if(CurrentColor == OldColor) {
							ReadTime = millis();
							ElapsedTime = ReadTime - StartTime;
							if(ElapsedTime > LimitTime) {
								monitor.println("No Rotation Detected");
								MotorStop();
								delay(2000);
								Step=0;
							}
							else {
								MotorUp();
							}
						}
						else {
							OldColor = CurrentColor;
							CurrentPosition++;
							StartTime = millis();
							MotorUp();
						}
					}
				}
				else {
				MotorStop();
					Step = 0;
				} 
				break; //Step:1
	  
				case 11:
					monitor.println("Performing MIDDLE sensor ON = Going UP");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition);  
					ReadSensors();
					if(OutputValueSensorMiddle < 300) {
						if(CurrentPosition == MaxPosition) {
							monitor.println("Current Position TOP LIMIT");
							MotorStop();
							delay(1000);
							DireccionSensorMiddle = 0;
							Step=0;
						}
						else {
							ReadColor = analogRead(A3);
							if(ReadColor < 100) {
								CurrentColor = 1;
							}
							else {
								CurrentColor = 0;
							}
							if(CurrentColor == OldColor) {
								ReadTime = millis();
								ElapsedTime = ReadTime - StartTime;
								if(ElapsedTime > LimitTime) {
									monitor.println("No Rotation Detected");
									MotorStop();
									delay(1000);
									DireccionSensorMiddle = 0;
									Step=0;
								}
								else {
									MotorUp();
								}
							}
							else {
								OldColor = CurrentColor;
								CurrentPosition++;
								StartTime = millis();
								MotorUp();
							}
						}
					}
				else if(OutputValueSensorMiddle > 300) {
					MotorStop();
					delay(1000);
					DireccionSensorMiddle = 0;
					Step = 0;
				} 
				break; //step :11
				case 12:
					monitor.println("Performing MIDDLE sensor ON = Going Down");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition);  
					ReadSensors();
				if(OutputValueSensorMiddle < 300) {
					if(CurrentPosition == 0) {
							MotorStop();
							delay(1000);
							monitor.println("Current Position GROUND limit");
							DireccionSensorMiddle = 1;
							Step=0;
					}
					else {
							ReadColor = analogRead(A3);
							if(ReadColor < 100) {
								CurrentColor = 1;
							}
							else {
								CurrentColor = 0;
							}
							if(CurrentColor == OldColor) {
								ReadTime = millis();
								ElapsedTime = ReadTime - StartTime;
								if(ElapsedTime > LimitTime) {
									monitor.println("No Rotation Detected");
									MotorStop();
									delay(1000);
                                          DireccionSensorMiddle = 1;
									Step=0;
								}
								else {
									MotorDown();
								}
							}
							else {
								OldColor = CurrentColor;
								CurrentPosition--;
								StartTime = millis();
								MotorDown();
							}
					}
				}
				else if(OutputValueSensorMiddle > 300) {
					MotorStop();
					delay(1000);
					DireccionSensorMiddle = 1;
					Step = 0;
				}
				break;//Step:12
	  
				case 21:  
					monitor.println("Performing Right sensor ON = Going Down");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition);  
					ReadSensors();
					if(OutputValueSensorRight > 300) {
						if(CurrentPosition == 0) {
							MotorStop();
							monitor.println("Current Position limit");
							Step=0;
						}
						else {
							MotorStop();
							ReadColor = analogRead(A3);
							if(ReadColor < 100) {
								CurrentColor = 1;
							}
							else {
								CurrentColor = 0;
							}
							if(CurrentColor == OldColor) {
								ReadTime = millis();
								ElapsedTime = ReadTime - StartTime;
								if(ElapsedTime > LimitTime) {
									monitor.println("No Rotation Detected");
									MotorStop();
									delay(2000);
									Step=0;
								}
								else {
								MotorDown();
								}
							}
							else {
								OldColor = CurrentColor;
								CurrentPosition--;
								StartTime = millis();
								MotorDown();
							}
						}
					}
					else {
						MotorStop();
						Step = 0;
					} 
				break; //Step:21
			}
		}
		
	}

int checkForNewPosition() {
	if (wifi.available() > 0) {
		int len = wifi.readBytesUntil('\n', buf, sizeof(buf));
		monitor.print("BUF %s");
		monitor.println(buf);

		if (!strncmp(buf, "pos=", 4)) {
			if (len > 4) {
				String pos = String(buf).substring(4, len);
				RequestedPosition = pos.toInt();
			}
		}
	}
	return RequestedPosition;
}


void WebRequest() {
	int position = (TurnCounter * 100)/MaxPosition;
	String path = "/position/front/top/" + String(position);
	//String domain = "192.168.43.247";
	String domain = "192.168.42.3";
	String port = "5000";

	//create start command
	String startcommand = "AT+CIPSTART=\"TCP\",\"" + domain + "\", " + port;

	//test for a start error
	if (wifi.find("Error")) {
		monitor.println("WIFIERROR");
		return;
	}

	//create the request command
	String sendcommand = "GET " + path + " HTTP/1.0\r\n\r\n\r\n";//works for most cases

	//send 
	wifi.print("AT+CIPSEND=");
	wifi.println(sendcommand.length());

	if (!wifi.find(">")) {
		wifi.println("AT+CIPCLOSE");
		delay(1000);
		return;
	}

	wifi.print(sendcommand); 
}


int MaxPositionFunction(int x, int y){
  int result;
  result = x * y;
  return result;
}

  
int DetectRotation() {
   ReadSensorIR = analogRead(SensorIR);
   ReadMotorUp = digitalRead(Motor1Pin);
   ReadMotorDown = digitalRead(Motor2Pin);
   if (ReadSensorIR < 100) {
      state = 1;
   }
   else {
      state = 0;
   } 
   
   if (state != oldstate && ReadMotorUp== HIGH) {
    TurnCounter++;
    start = millis();
  }
  else if(state != oldstate && ReadMotorDown== HIGH){
     TurnCounter--;
     start = millis();
  }
      //TurnCounter++;
    //  start = millis();
     //delay(500);


 
   else {
      finished=millis();
     // monitor.println("Finished");
      elapsed=finished-start; 
      if (elapsed > 1500) {
        monitor.println("");
         monitor.print("No Rotation ");
         monitor.print(elapsed);
  monitor.println(" milliseconds elapsed");
  monitor.println();
  delay(250);
 // elapsed=0;
         return false;
      }  
   }
   oldstate=state;
   return true;
 //  elapsed=0;
}

void MotorUp() {
  digitalWrite(Motor1Pin, HIGH); // set leg 1 of the H-bridge low
  digitalWrite(Motor2Pin, LOW); // set leg 2 of the H-bridge high
}

void MotorDown() {
  digitalWrite(Motor1Pin, LOW); // set leg 1 of the H-bridge low
  digitalWrite(Motor2Pin, HIGH); // set leg 2 of the H-bridge high
}

void MotorStop() {
  digitalWrite(Motor1Pin, LOW); // set leg 1 of the H-bridge low
  digitalWrite(Motor2Pin, LOW); // set leg 2 of the H-bridge high
}



void ReadSensors(){
 ValueSensorRight = analogRead (SensorRight);
 OutputValueSensorRight = map(ValueSensorRight, 0, 1023, 400, 0);
 ValueSensorMiddle = analogRead (SensorMiddle);
 OutputValueSensorMiddle = map(ValueSensorMiddle, 0, 1023, 400, 0);
 ValueSensorLeft = analogRead (SensorLeft);
 OutputValueSensorLeft = map(ValueSensorLeft, 0, 1023, 400, 0);
 ValueSensorIR = analogRead (SensorIR);
 //OutputValueSensorIR = map(ValueSensorIR, 0, 1023, 255, 0);
}

void PrintInfo() {
  if(Motor1Pin== HIGH){
    monitor.println("Motor going up"); //For Debugging
  }
  if(Motor2Pin== LOW && Motor1Pin == LOW){
     monitor.println("Motor Stop"); //For Debugging
  }
  if(Motor2Pin== HIGH){
     monitor.println("Motor going down"); //For Debugging
  }
   
  monitor.print("MaxCount:"); //For Debugging
  monitor.println(MaxCount); //For Debugging
   monitor.print("Position:"); //For Debugging
  monitor.println(Position); //For Debugging
   monitor.print("IRsensor:"); //For Debugging
  monitor.println(ValueSensorIR); //For Debuggin
   monitor.print("Sensorright:"); //For Debugging
  monitor.println(OutputValueSensorRight); //For Debuggin
   monitor.print("SensorLeft:"); //For Debugging
  monitor.println(OutputValueSensorLeft); //For Debuggin
   monitor.print("SensorMiddle:"); //For Debugging
  monitor.println(OutputValueSensorMiddle); //For Debuggin
}
