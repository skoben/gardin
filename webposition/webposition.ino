//#include <SoftwareSerial.h>

#define wifi Serial1
#define monitor Serial2
//SoftwareSerial monitor(11, 10); // RX, TX

#define WIFI_SSID "hejhej"
#define WIFI_PASSWD "ingenting"
#define SERVER_DOMAIN "192.168.43.108"

#define WINDOW_SIDE "front"
#define WINDOW_END "top"

#define INIT_STATE 0
#define START_COLOR_READING_STATE 1
#define TOP_REACHED_STATE 2
#define COUNT_TURNS_STATE 3
#define WAIT_FOR_NEW_POSITION_STATE 4
#define GO_UP_STATE 5
#define GO_DOWN_STATE 6


//Position 
int Position;
int NewPosition;
int RequestedPosition = 0;
int MaxPosition = 0; 
int MinPosition = 0;
int inbyte;
int digitalState;
int pinNumber;
int analogRate;
int sensorVal;

char buf[256];

//erik
int CurrentColor, OldColor, ReadColor, CurrentPosition, SensorMiddleValue;
unsigned long StartTime, ReadTime, ElapsedTime, LimitTime;


int TurnCounter;
unsigned long start, finished, elapsed;
int oldstate = 0;
int state = 0;
int Step = 0; // READY program case-switch funtion
int Start = INIT_STATE;// START Mätningsprogram 
//Sensors
int SensorLeft = A0;
int SensorMiddle = A1;
int SensorRight = A2;
int SensorIR = A3;
int ValueSensorRight;
int ValueSensorMiddle;
int ValueSensorLeft;
static int DireccionSensorMiddle = 1;
int ValueSensorIR;
int OutputValueSensorRight;
int OutputValueSensorMiddle;
int OutputValueSensorLeft;
int OutputValueSensorIR;
static int MaxCount = 0;
int ReadSensorIR;
int ReadMotorUp;
int ReadMotorDown;
//Motors
int Motor1Pin = 5;
int Motor2Pin = 6;
int Rotation;
bool calibrationFinished = false;



void setup() {
	monitor.begin(19200);
	wifi.begin(19200);
	wifi.println("AT+RST");
	wifi.println("AT+CWMODE=1");
	delay(500);
	wifi.println("AT+RST");

	//connect to wifi network
	wifi.println("AT+CWJAP=\"" WIFI_SSID "\",\"" WIFI_PASSWD "\"");
	delay(5000);
	while (wifi.available() > 0) {
		monitor.write(wifi.read());
	}

	LimitTime = 1500;
	start = millis();
	pinMode(Motor1Pin, OUTPUT);
	pinMode(Motor2Pin, OUTPUT);
	pinMode(SensorIR, INPUT);
	pinMode(SensorRight,INPUT); // analog input
	pinMode(SensorMiddle,INPUT);
	pinMode(SensorLeft,INPUT);
}

void loop() {
	if (calibrationFinished) {
		if (millis() % 5000 == 0) {
			monitor.println("WEBREQUEST");
			WebRequest();
		}
		checkForNewPosition();
	}
	
	switch(Start) {           	
    case INIT_STATE:
		monitor.println("Performing Start 0");
		ReadColor = analogRead(A3);
		if(ReadColor < 10) {
			OldColor = 1;
		} else {
			OldColor = 0;
		}
		MotorUp();
		StartTime = millis();
		Start = START_COLOR_READING_STATE;
		break;
    case START_COLOR_READING_STATE:
//		MotorStop();
		ReadColor = analogRead(A3);
		if (ReadColor < 100) {
			CurrentColor = 1;
		} else {
			CurrentColor = 0;
		}
		if (OldColor == CurrentColor) {
			ReadTime = millis();
			ElapsedTime = ReadTime - StartTime;
			if (ElapsedTime > LimitTime) {            
				MotorStop();
				monitor.println("No Rotation Detected!"); 
				monitor.print("Elapsed Time: ");
				monitor.println(ElapsedTime);
				delay(3000);
				Start = TOP_REACHED_STATE;
			} else {
				MotorUp();
			}
		} else {
			OldColor = CurrentColor;
			StartTime = millis();
			MotorUp();
		}
		break;
	case TOP_REACHED_STATE:
		monitor.println("Performing Start 2");
		ReadColor = analogRead(A3);
		if(ReadColor < 100) {
			OldColor = 1;
		} else {
			OldColor = 0;
		}
		MotorDown();
		StartTime = millis();
		Start = COUNT_TURNS_STATE; 
		break;
    case COUNT_TURNS_STATE:
        //monitor.println("Performing Start 3");

//	        MotorStop();
        ReadColor = analogRead(A3);
		//        if (millis() % 500 == 0) {
			monitor.print("MaxPosition: ");
			monitor.print(ReadColor);
			monitor.print(", ");
			monitor.println(MaxPosition);
//		}
        if (ReadColor < 100) {
            CurrentColor = 1;
         } else {
            CurrentColor = 0;
        }
        if (OldColor == CurrentColor) {
            ReadTime = millis();
            ElapsedTime = ReadTime - StartTime;
            if (ElapsedTime > LimitTime) {            
				MotorStop();
				//delay(2000);
				monitor.println("No Rotation Detected!"); 
				monitor.print("Elapsed Time: ");
				monitor.println(ElapsedTime);
				CurrentPosition = 0;
				Start = WAIT_FOR_NEW_POSITION_STATE;
				calibrationFinished = true;
				monitor.print("MaxPosition: ");
				monitor.println(MaxPosition);	
            } else {
				MotorDown();
            }
        } else {
            OldColor = CurrentColor;
            StartTime = millis();
            MaxPosition++;
            MotorDown();
         }
		break;
    case WAIT_FOR_NEW_POSITION_STATE:
		//monitor.println("Start 4: Ready ");
		//Position = TurnCounter;
		//monitor.print("MaxPosition:"); //For Debugging
		//monitor.println(MaxPosition); //For Debugging
		//monitor.print("CurrentPosition:"); //For Debugging
		//monitor.println(CurrentPosition); //For Debugging
		checkForNewPosition();
		if (RequestedPosition <= 100){
			NewPosition = MaxPosition * (RequestedPosition * 0.01);
			if (NewPosition >= 0) {
				if (CurrentPosition < NewPosition) {
					MotorStop();
					ReadColor = analogRead(A3);
					if(ReadColor > 300) {
						OldColor = 1;
					}
					else {
						OldColor = 0;
					}
					monitor.print(" Going UP to new position: "); //For Debugging
					monitor.println(NewPosition);
					MotorUp();
					StartTime = millis();
					Start = GO_UP_STATE;
					
					monitor.println("Going UP to new position and counting");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition); 
					monitor.print("NewPosition: ");
					monitor.println(NewPosition);		 

					}
				if (CurrentPosition > NewPosition) { 
					MotorStop();
					ReadColor = analogRead(A3);
					if (ReadColor > 300) {
						OldColor = 1;
					}
					else {
						OldColor = 0;
					}
					monitor.print("Going down to new position:"); //For Debugging
					monitor.println(NewPosition);
					MotorDown();
					StartTime = millis();
					Start = GO_DOWN_STATE;
					
					monitor.println("Going UP to new position and counting");
					monitor.print("MaxPosition: ");
					monitor.println(MaxPosition);
					monitor.print("CurrentPosition: ");
					monitor.println(CurrentPosition); 
					monitor.print("NewPosition: ");
					monitor.println(NewPosition);		 

				}
			}
		} else if (RequestedPosition == 101) {
			Start = 7;
		} else {
			Start = WAIT_FOR_NEW_POSITION_STATE;
		}
		break;

	case GO_UP_STATE:
		if (millis() % 2000 == 0) {
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);			
		}
		if (CurrentPosition == MaxPosition) {
		   MotorStop();
		   monitor.println("Current Position Top limit");  
			Start = WAIT_FOR_NEW_POSITION_STATE;
		} else {
			MotorStop();
			ReadColor = analogRead(A3);
			if (ReadColor < 100) {
				CurrentColor = 1;
			} else {
				CurrentColor = 0;
			}
			if (CurrentColor == OldColor) {
				ReadTime = millis();
				ElapsedTime = ReadTime - StartTime;
				if (ElapsedTime > LimitTime) {
					monitor.println("No Rotation Detected");
					MotorStop();
					delay(2000);
					Start = WAIT_FOR_NEW_POSITION_STATE;
				} else {
					MotorUp();
				}
			} else {
				OldColor = CurrentColor;
				CurrentPosition++;
				StartTime = millis();
				MotorUp();
			}
		}
		if (CurrentPosition == NewPosition) {
			monitor.println(" Position == New Position");
			MotorStop();			
			Start = WAIT_FOR_NEW_POSITION_STATE;
		}
		break; //Start:5

	case GO_DOWN_STATE:
		if (millis() % 2000 == 0) {
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);			
		}

		if (CurrentPosition == 0) {
			MotorStop();
			monitor.println("Current Position ground limit");  
			Start = WAIT_FOR_NEW_POSITION_STATE;
		} else {
			MotorStop();
			ReadColor = analogRead(A3);
			if (ReadColor < 100) {
				CurrentColor = 1;
			} else {
			  CurrentColor = 0;
			}
			if (CurrentColor == OldColor) {
				ReadTime = millis();
				ElapsedTime = ReadTime - StartTime;
				if (ElapsedTime > LimitTime) {
					monitor.println("No Rotation Detected");
					MotorStop();
					delay(2000);
					Start = WAIT_FOR_NEW_POSITION_STATE;
				} else {
					MotorDown();
				}
			} else {
				OldColor = CurrentColor;
				CurrentPosition--;
				StartTime = millis();
				MotorDown();
			}
		}
	    if (CurrentPosition == NewPosition) {
			monitor.println(" Position == New Position");
			MotorStop();					
			Start = WAIT_FOR_NEW_POSITION_STATE;
		}
		break; //Start:6
			
	case 7:
		monitor.println("Step 0: Ready ");
		Position = TurnCounter;
		monitor.print("MaxPosition:"); //For Debugging
		monitor.println(MaxPosition); //For Debugging
		monitor.print("CurrentPosition:"); //For Debugging
		monitor.println(CurrentPosition); //For Debugging

		switch(Step) {           	
		case 0:		// 	Startrutin	
			ReadSensors();
			if (OutputValueSensorLeft > 300) { //SENSOR LEFT STEPS
				if (CurrentPosition == MaxPosition) {
					MotorStop();
					monitor.println("Current Position Top limit");
					Step = 0;
				} else {
					MotorStop();
					ReadColor = analogRead(A3);
					if(ReadColor > 300) {
						OldColor = 1;
					} else {
						OldColor = 0;
					}
					MotorUp();
					StartTime = millis();
					Step = 1;
				}
			} else if (OutputValueSensorMiddle > 300 && DireccionSensorMiddle == 1) { //SENSOR MIDDLE STEPS
				delay(500);
				if (CurrentPosition == MaxPosition) {
					MotorStop();
					monitor.println("Current Position Top limit");
					DireccionSensorMiddle = 0;
					Step = 12;
				} else {
					//MotorStop();
					ReadColor = analogRead(A3);
					if (ReadColor > 300) {
						OldColor = 1;
					} else {
						OldColor = 0;
					}
					MotorUp();
					StartTime = millis();
					Step = 11;
				}
			} else if(OutputValueSensorMiddle > 300 && DireccionSensorMiddle == 0) { //SENSOR MIDDLE STEPS
				delay(500);
				if(CurrentPosition == 0) {
					MotorStop();
					monitor.println("Current Position  ground Limit");
					DireccionSensorMiddle = 1;
					Step = 11;
				} else {
					MotorStop();
					ReadColor = analogRead(A3);
					if(ReadColor > 300) {
						OldColor = 1;
					} else {
						OldColor = 0;
					}
					MotorDown();
					StartTime = millis();
					Step = 12;
				}                 
			} else if (OutputValueSensorRight > 300) { //SENSOR RIGHT STEPS
				if (CurrentPosition == 0) {
					MotorStop();
					monitor.println("Current Position ground Limit");
					Step = 0;
				} else {
					MotorStop();
					ReadColor = analogRead(A3);
					if (ReadColor > 300) {
						OldColor = 1;
					} else {
						OldColor = 0;
					}
					MotorUp();
					StartTime = millis();
					Step = 21;
				}
			} else if (OutputValueSensorRight < 10 &&  OutputValueSensorLeft < 10) { //SENSOR RIGHT and sensor left are blackned
				delay(2000);
				Start = WAIT_FOR_NEW_POSITION_STATE;
			} else {
				monitor.println(" No Light Detected");
			}     
			break; // case : 0
	  
		case 1:   
			monitor.println("Performing Left sensor ON = Going UP");
			monitor.print("MaxPosition: ");
			monitor.println(MaxPosition);
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);  
			ReadSensors();
			if (OutputValueSensorLeft > 300) {
				if (CurrentPosition == MaxPosition) {
					MotorStop();
					monitor.println("Current Position limit");  
					Step=0;
				} else {
					MotorStop();
					ReadColor = analogRead(A3);
					if (ReadColor < 100) {
						CurrentColor = 1;
					} else {
						CurrentColor = 0;
					}
					if (CurrentColor == OldColor) {
						ReadTime = millis();
						ElapsedTime = ReadTime - StartTime;
						if (ElapsedTime > LimitTime) {
							monitor.println("No Rotation Detected");
							MotorStop();
							delay(2000);
							Step = 0;
						} else {
							MotorUp();
						}
					} else {
						OldColor = CurrentColor;
						CurrentPosition++;
						StartTime = millis();
						MotorUp();
					}
				}
			} else {
				MotorStop();
				Step = 0;
			}
			break; //Step:1
	  
		case 11:
			monitor.println("Performing MIDDLE sensor ON = Going UP");
			monitor.print("MaxPosition: ");
			monitor.println(MaxPosition);
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);  
			ReadSensors();
			if (OutputValueSensorMiddle < 300) {
				if (CurrentPosition == MaxPosition) {
					monitor.println("Current Position TOP LIMIT");
					MotorStop();
					delay(1000);
					DireccionSensorMiddle = 0;
					Step = 0;
				} else {
					ReadColor = analogRead(A3);
					if (ReadColor < 100) {
						CurrentColor = 1;
					} else {
						CurrentColor = 0;
					}
					if (CurrentColor == OldColor) {
						ReadTime = millis();
						ElapsedTime = ReadTime - StartTime;
						if (ElapsedTime > LimitTime) {
							monitor.println("No Rotation Detected");
							MotorStop();
							delay(1000);
							DireccionSensorMiddle = 0;
							Step = 0;
						} else {
							MotorUp();
						}
					} else {
						OldColor = CurrentColor;
						CurrentPosition++;
						StartTime = millis();
						MotorUp();
					}
				}
			} else if (OutputValueSensorMiddle > 300) {
				MotorStop();
				delay(1000);
				DireccionSensorMiddle = 0;
				Step = 0;
			}
			break; //step :11
		case 12:
			monitor.println("Performing MIDDLE sensor ON = Going Down");
			monitor.print("MaxPosition: ");
			monitor.println(MaxPosition);
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);  
			ReadSensors();
			if (OutputValueSensorMiddle < 300) {
				if (CurrentPosition == 0) {
					MotorStop();
					delay(1000);
					monitor.println("Current Position GROUND limit");
					DireccionSensorMiddle = 1;
					Step = 0;
				} else {
					ReadColor = analogRead(A3);
					if(ReadColor < 100) {
						CurrentColor = 1;
					}
					else {
						CurrentColor = 0;
					}
					if (CurrentColor == OldColor) {
						ReadTime = millis();
						ElapsedTime = ReadTime - StartTime;
						if (ElapsedTime > LimitTime) {
							monitor.println("No Rotation Detected");
							MotorStop();
							delay(1000);
							DireccionSensorMiddle = 1;
							Step = 0;
						}
						else {
							MotorDown();
						}
					}
					else {
						OldColor = CurrentColor;
						CurrentPosition--;
						StartTime = millis();
						MotorDown();
					}
				}
			} else if(OutputValueSensorMiddle > 300) {
				MotorStop();
				delay(1000);
				DireccionSensorMiddle = 1;
				Step = 0;
			}
			break;//Step:12

		case 21:  
			monitor.println("Performing Right sensor ON = Going Down");
			monitor.print("MaxPosition: ");
			monitor.println(MaxPosition);
			monitor.print("CurrentPosition: ");
			monitor.println(CurrentPosition);  
			ReadSensors();
			if (OutputValueSensorRight > 300) {
				if (CurrentPosition == 0) {
					MotorStop();
					monitor.println("Current Position limit");
					Step = 0;
				} else {
					MotorStop();
					ReadColor = analogRead(A3);
					if (ReadColor < 100) {
						CurrentColor = 1;
					} else {
						CurrentColor = 0;
					}
					if (CurrentColor == OldColor) {
						ReadTime = millis();
						ElapsedTime = ReadTime - StartTime;
						if (ElapsedTime > LimitTime) {
							monitor.println("No Rotation Detected");
							MotorStop();
							delay(2000);
							Step=0;
						} else {
							MotorDown();
						}
					} else {
						OldColor = CurrentColor;
						CurrentPosition--;
						StartTime = millis();
						MotorDown();
					}
				}
			} else {
				MotorStop();
				Step = 0;
			} 
			break; //Step:21
		}
	}	
}

int checkForNewPosition() {
	if (wifi.available() > 0) {
		int len = wifi.readBytesUntil('\n', buf, sizeof(buf));
		//monitor.print("BUF: ");
        //monitor.print(len);
		//monitor.print(buf);
        //monitor.println("\n====");

		if (!strncmp(buf, "pos=", 4)) {
			if (len > 4) {
				String pos = String(buf).substring(4, len);
				RequestedPosition = pos.toInt();
				monitor.print("\nREQUESTED POS: ");
				monitor.println(RequestedPosition);
			}
		}
	}
	
	return RequestedPosition;
}


void WebRequest() {
    int position = (CurrentPosition * 100) / MaxPosition;
    if (position < 0) {
		position = 0;
	}
	monitor.println("CURR: " + String(CurrentPosition) + ", MAX: " + String(MaxPosition) + ", POS%: " + String(position) + ", NEW: " + String(NewPosition));
    String path = "/position/" WINDOW_SIDE "/" WINDOW_END "/";
    path += String(position);

    //create start command
    String startcommand = "AT+CIPSTART=\"TCP\",\"" SERVER_DOMAIN "\", 80";
     
    wifi.println(startcommand);
    //monitor.println(startcommand);
          
    //test for a start error
    if (wifi.find("Error")){
		monitor.println("error on start");
		return;
    }
     
     //create the request command
    String sendcommand = "GET http://" SERVER_DOMAIN; 
	sendcommand += path + " HTTP/1.0\r\n\r\n\r\n";
     
    monitor.print(sendcommand);
     
     //send 
    wifi.print("AT+CIPSEND=");
    wifi.println(sendcommand.length());
     
    //debug the command
    monitor.print("AT+CIPSEND=");
    monitor.println(sendcommand.length());
     
    //delay(5000);
    if (wifi.find(">")){
		monitor.println(">");
    } else {
		wifi.println("AT+CIPCLOSE");
		monitor.println("connect timeout");
		delay(1000);
		return;
	}

	//monitor.print(getcommand);
	wifi.print(sendcommand);
}


int MaxPositionFunction(int x, int y) {
	int result;
	result = x * y;
	return result;
}

void MotorUp() {
//	monitor.println("MOTOR UP");
	digitalWrite(Motor1Pin, HIGH); // set leg 1 of the H-bridge low
	digitalWrite(Motor2Pin, LOW); // set leg 2 of the H-bridge high
}

void MotorDown() {
//	monitor.println("MOTOR DOWN");
	digitalWrite(Motor1Pin, LOW); // set leg 1 of the H-bridge low
	digitalWrite(Motor2Pin, HIGH); // set leg 2 of the H-bridge high
}

void MotorStop() {
//	monitor.println("MOTOR STOP");
	digitalWrite(Motor1Pin, LOW); // set leg 1 of the H-bridge low
	digitalWrite(Motor2Pin, LOW); // set leg 2 of the H-bridge high
}



void ReadSensors() {
	ValueSensorRight = analogRead (SensorRight);
	OutputValueSensorRight = map(ValueSensorRight, 0, 1023, 400, 0);
	ValueSensorMiddle = analogRead (SensorMiddle);
	OutputValueSensorMiddle = map(ValueSensorMiddle, 0, 1023, 400, 0);
	ValueSensorLeft = analogRead (SensorLeft);
	OutputValueSensorLeft = map(ValueSensorLeft, 0, 1023, 400, 0);
	ValueSensorIR = analogRead (SensorIR);
	//OutputValueSensorIR = map(ValueSensorIR, 0, 1023, 255, 0);
}

void PrintInfo() {
	if (Motor1Pin == HIGH) {
		monitor.println("Motor going up"); //For Debugging
	}
	if (Motor2Pin == LOW && Motor1Pin == LOW) {
		monitor.println("Motor Stop"); //For Debugging
	}
	if (Motor2Pin == HIGH) {
		monitor.println("Motor going down"); //For Debugging
	}
   
	monitor.print("MaxCount:"); //For Debugging
	monitor.println(MaxCount); //For Debugging
	monitor.print("IRsensor:"); //For Debugging
	monitor.println(ValueSensorIR); //For Debuggin
	monitor.print("Sensorright:"); //For Debugging
	monitor.println(OutputValueSensorRight); //For Debuggin
	monitor.print("SensorLeft:"); //For Debugging
	monitor.println(OutputValueSensorLeft); //For Debuggin
	monitor.print("SensorMiddle:"); //For Debugging
	monitor.println(OutputValueSensorMiddle); //For Debuggin
}