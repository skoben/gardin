#include <SoftwareSerial.h>

SoftwareSerial monitor(11, 10); // RX, TX
#define wifi Serial

void setup()
{
	wifi.begin(115200);//connection to ESP8266
	monitor.begin(9600); //serial debug


	//set mode needed for new boards
	wifi.println("AT+RST");
	wifi.println("AT+CWMODE=1");
	delay(500);//delay after mode change
	wifi.println("AT+RST");

	//connect to wifi network
	wifi.println("AT+CWJAP=\"hejhej\",\"ingenting\"");
}


int cc = 0;
char buf[256];

void loop()
{
	//output everything from ESP8266 to the Arduino Micro monitor output
	if (wifi.available() > 0) {
		int len = wifi.readBytesUntil('\n', buf, sizeof(buf));

		if (!strncmp(buf, "pos=", 4)) {
			if (len > 4) {
				String pos = String(buf).substring(4, len);
				monitor.print("POS: ");
				monitor.println(pos);
			}
		}
		monitor.println("XXX");
		buf[len] = '\0';
		monitor.println(buf);
	}

	if (millis() % 5000 == 0) {
		monitor.println("WEB REQUEST");
		web_request();
	}
}


void web_request()
{
	String path = "/position/front/top/90";
	String domain = "192.168.43.247";
	String port = "5000";

	//create start command
	String startcommand = "AT+CIPSTART=\"TCP\",\"" + domain + "\", " + port;

	wifi.println(startcommand);

	//test for a start error
	if (wifi.find("Error")) {
		monitor.println("error on start");
		return;
	}

	//create the request command
	String sendcommand = "GET " + path + " HTTP/1.0\r\n\r\n\r\n";//works for most cases

	monitor.print(sendcommand);

	//send 
	wifi.print("AT+CIPSEND=");
	wifi.println(sendcommand.length());

	//debug the command
	monitor.print("AT+CIPSEND=");
	monitor.println(sendcommand.length());

		delay(1000);
	if (wifi.find(">")) {
		monitor.println(">");
	} else {
		wifi.println("AT+CIPCLOSE");
		monitor.println("connect timeout");
		delay(1000);
		return;
	}

	wifi.print(sendcommand); 
}
